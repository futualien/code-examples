<?php

namespace App\Services;

use Illuminate\Support\Str;

abstract class ModelService
{
    protected $model;

    public function model()
    {
        return $this->model;
    }

    public function __call($name, $arguments)
    {
        return $this->model::$name($arguments[0])->toArray();
    }

    public function rules(string $action = null): array
    {
        return (new $this->model)->rules($action);
    }

    public function all(): array
    {
        return $this->model::all()->toArray();
    }

    public function index(): array
    {
        return $this->all();
    }

    public function show($key): array
    {
        return $this->model::findOrFail($key)->toArray();
    }

    public function delete($key): bool
    {
        $model = $this->model::withoutGlobalScopes()->findOrFail($key);
        return $model->delete();
    }

    public function update($key, array $attributes): array
    {
        $model = $this->model::withoutGlobalScopes()->findOrFail($key);
        $model->fill($attributes);
        $model->save();
        return $model->toArray();
    }

    public function create(array $attributes): array {
        return $this->model::create($attributes)->toArray();
    }
}
