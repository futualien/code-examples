<?php

namespace App\Services;

use App\Models\Command;
use App\Models\Device;
use App\Models\DeviceCommand;
use App\Models\DeviceEvent;
use Exception;
use Illuminate\Support\Facades\Storage;
use InvalidArgumentException;

class CommandService extends ModelService
{
    protected $model = Command::class;

    public function index(): array
    {
        return $this->model::active()->get()->toArray();
    }

    public function getDeviceCommands(int $deviceId = null): array
    {
        if (empty($deviceId)) {
            return DeviceCommand::all()->toArray();
        }
        $device = Device::findOrFail($deviceId);
        return $device->deviceCommands()->with(['command', 'user'])
            ->latest()
            ->get()
            ->transform(static function ($command) {
                if (!empty($command->data['files'])) {
                    $data = $command->data;
                    foreach ($data['files'] as $key => $file) {
                        $data['files'][$key] = Storage::url($file);
                    }
                    $command->data = $data;
                }
                return $command;
            })
            ->toArray();
    }

    public function getNewDeviceCommands(int $deviceId): array
    {
        $device = Device::findOrFail($deviceId);
        $deviceCommands = $device->deviceCommands()
            ->unsent()
            ->notCanceled()
            ->with(['command'])
            ->oldest()
            ->get();

        $deviceCommands->each(function ($deviceCommand) {
            $deviceCommand->markAsSent()->save();
        });

        return $deviceCommands->transform(function ($deviceCommand) {
            return [
                'commandId' => $deviceCommand->id,
                'command' => $deviceCommand->command->alias,
                'commandBody' => $deviceCommand->command_body
            ];
        })->toArray();
    }

    public function createDeviceCommand(array $data): array
    {
        if (empty($data['user_id'])) {
            $data['user_id'] = auth()->id();
        }
        return DeviceCommand::create($data)->toArray();
    }

    public function createDeviceCommands(array $data): array
    {
        $commands = [];
        foreach ($data['commands'] as $commandData) {
            $commands[] = $this->createDeviceCommand($commandData);
        }
        return $commands;
    }

    public function deleteDeviceCommand($deviceCommandId): bool
    {
        $deviceCommand = DeviceCommand::findOrFail($deviceCommandId);
        return $deviceCommand->delete();
    }

    public function getDeviceCommandRules($action = null): array
    {
        return (new DeviceCommand)->rules($action);
    }

    public function registerEvent(array $data)
    {
        $type = $this->detectEventType($data);
        switch ($type) {
            case DeviceEvent::TYPE_COMMAND_EXECUTED:
                $deviceCommand = DeviceCommand::findOrFail($data['command_id']);

                if (isset($data['data']['files'])) {
                    logger()->debug($data['data']['files']);
                    $deviceId = auth()->id();
                    foreach ($data['data']['files'] as $key => &$file) {
                        $dir = app('Str')->plural($key);
                        $file = Storage::disk('public')
                            ->put("/devices/$deviceId/$dir", $file);
                    }
                    $deviceCommand->setData($data['data']);
                }

                $result = $deviceCommand
                    ->setResult($data['status'])
                    ->setMessage($data['message'])
                    ->markAsStarted($data['started_at'])
                    ->markAsFinished($data['finished_at'])
                    ->save();
                break;
            default:
                $data['type'] = $type;
                $result = $this->createEvent($data)->toArray();
        }
        return $result;
    }


    protected function detectEventType(array $data): int
    {
        if (empty($data['type'])) {
            return DeviceEvent::TYPE_OTHER;
        }
        if ($data['type'] === DeviceEvent::TYPE_OFFLINE) {
            return DeviceEvent::TYPE_OFFLINE;
        }
        return DeviceEvent::TYPES[$data['type']] ?? DeviceEvent::TYPE_OTHER;
    }

    protected function createEvent(array $data): DeviceEvent
    {
        $deviceId = $data['device_id'] ?? auth()->id();
        throw_if(empty($deviceId), new InvalidArgumentException('Device id required'));

        $hasEnabledDevice = Device::id($deviceId)->enabled()->exists();
        throw_if(!$hasEnabledDevice, new Exception('Device must be enabled'));

        return DeviceEvent::create([
            'device_id' => $deviceId,
            'type' => $data['type'] ?? DeviceEvent::TYPE_OTHER,
            'message' => $data['message'],
            'code' => $data['error'] ?? null,
            'duration' => $data['duration'] ?? null,
            'started_at' => $data['started_at'] ?? null,
        ]);
    }

    public function cancelDeviceCommand($key): array
    {
        $model = DeviceCommand::unsent()->findOrFail($key);
        $model->cancel()->save();
        return $model->toArray();
    }
}
