<?php

namespace App\Models;

use App\Events\DeviceActivated;
use App\Traits\Filterable;
use App\Traits\HasRules;
use App\Traits\Searchable;
use App\Traits\TableColumns;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Carbon\Carbon;

/**
 * @property string|null register_code
 * @property bool is_registered
 * @property bool is_enabled
 * @property integer user_id
 * @property string access_token
 * @property \Illuminate\Support\Carbon registered_at
 */
class Device extends Authenticatable implements JWTSubject
{
    use HasRules, SoftDeletes, TableColumns, Filterable, Searchable;

    protected $fillable = ['name', 'address', 'uuid', 'is_enabled', 'project_id', 'partner_id', 'register_code', 'activation_code', 'latitude', 'longitude'];

    protected $dates = ['created_at', 'updated_at', 'registered_at'];

    protected $casts = [
        'is_enabled' => 'boolean',
        'is_registered' => 'boolean',
    ];

    protected $rules = [
        'create' => [
            'name' => 'required|string|max:255',
            'address' => 'sometimes|string|max:255',
            'project_id' => 'sometimes|integer|exists:projects,id',
            'partner_id' => 'sometimes|integer|exists:partners,id',
            'latitude' => 'sometimes|nullable|numeric',
            'longitude' => 'sometimes|nullable|numeric'
        ],
        'update' => [
            'name' => 'sometimes|string|max:255',
            'address' => 'sometimes|string|max:255',
            'project_id' => 'sometimes|integer|exists:projects,id',
            'latitude' => 'sometimes|nullable|numeric',
            'longitude' => 'sometimes|nullable|numeric'
        ]
    ];

    protected $filterable = [];

    protected $searchable = [
        'name',
        ['relation' => 'partner']
    ];

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->is_enabled = false;

            if (empty($model->uuid)) {
                $model->register_code = Str::random(6);
                $model->is_registered = true;
            } elseif (empty($model->partner_id)) {
                $model->activation_code = Str::random(6);
                $model->is_registered = false;
            } else {
                $model->is_registered = false;
            }
        });

        self::saved(function ($model) {
            if ($model->isDirty('is_registered') && $model->is_registered) {
                event(new DeviceActivated($model));
            }
        });
    }

    protected static function booted()
    {
        parent::booted();
        static::addGlobalScope('RegisteredDevice', function ($q) {
            $q->registered();
        });
        static::addGlobalScope('AuthUserPartnerDevices', function ($q) {
            $q->when(auth()->check() && auth()->user() instanceof User && !auth()->user()->isAdmin(), function ($q) {
                $q->ofAuthUserPartner();
            });
        });
    }

    public function setUuid(string $uuid)
    {
        if (empty($this->uuid)) {
            $this->uuid = $uuid;
        }
        return $this;
    }

    public function setProject(?int $projectId)
    {
        $this->project_id = $projectId;
        return $this;
    }

    public function setPartner(?int $partnerId)
    {
        $this->partner_id = $partnerId;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->lastLog ? $this->lastLog->status : 0;
    }

    public function getDeviceStatus(): bool
    {
        return $this->is_enabled;
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getVPNDeviceName()
    {
        return 'device-' . sprintf('%06d', $this->id);
    }

    public function partner()
    {
        return $this->belongsTo(Partner::class, 'partner_id', 'id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    public function config()
    {
        return $this->hasOne(DeviceConfig::class, 'device_id', 'id');
    }

    public function logs()
    {
        return $this->hasMany(DeviceLog::class, 'device_id', 'id');
    }

    public function lastLog()
    {
        return $this->hasOne(DeviceLog::class, 'device_id', 'id')->latest()->orderBy('id', 'desc');
    }

    public function commands()
    {
        return $this->belongsToMany(Command::class, 'device_commands', 'device_id', 'command_id', 'id')->withTimestamps();
    }

    public function deviceCommands()
    {
        return $this->hasMany(DeviceCommand::class, 'device_id', 'id');
    }

    public function events()
    {
        return $this->hasMany(DeviceEvent::class, 'device_id', 'id');
    }

    public function lastEvent()
    {
        return $this->hasOne(DeviceEvent::class, 'device_id', 'id')->createdByDevices()->latest()->orderBy('id', 'desc');
    }

    public function scopeRegistered($q, bool $value = true)
    {
        return $q->withoutGlobalScope('RegisteredDevice')->where('is_registered', $value);
    }

    public function scopeUnregistered($q)
    {
        return $q->withoutGlobalScope('RegisteredDevice')->registered(false);
    }

    public function scopeOfPartner($q, $partnerId)
    {
        return $q->where('partner_id', $partnerId);
    }

    public function scopeOfAuthUserPartner($q)
    {
        $user = auth()->user();
        $userHasPartner = !empty($user->partner_id);
        $userIsAdmin = $user instanceof User && $user->isAdmin();
        return $q->when($userHasPartner && !$userIsAdmin, function ($q) use ($user) {
            $q->ofPartner($user->partner_id)->ofProjects($user->projects->pluck('id')->toArray());
        });
    }

    public function scopeOfProject($q, $projectId)
    {
        return $q->where('project_id', $projectId);
    }

    public function scopeOfProjects($q, $projects)
    {
        return $q->whereIn('project_id', $projects);
    }

    public function scopeEnabled($q, bool $value = true)
    {
        return $q->where('is_enabled', $value);
    }

    public function scopeOnline($q)
    {
        $offlineTime = config('device.offline_time', 15);
        $timeLine = now()->subMinutes($offlineTime);
        return $q->whereHas('logs', function ($q) use ($timeLine) {
            $q->createdAfter($timeLine);
        })->orWhereHas('events', function ($q) use ($timeLine) {
            $q->createdByDevices()->createdAfter($timeLine);
        });
    }

    public function scopeOffline($q)
    {
        $offlineTime = config('device.offline_time', 15);
        $timeLine = now()->subMinutes($offlineTime);
        return $q->whereDoesntHave('logs', function ($q) use ($timeLine) {
            $q->createdAfter($timeLine);
        })->whereDoesntHave('events', function ($q) use ($timeLine) {
            $q->createdByDevices()->createdAfter($timeLine);
        });
    }

    public function scopeNew($q)
    {
        return $q->where('created_at', '>', now()->subDays(3)->toDateTimeString());
    }

    public function scopeId($q, $id)
    {
        return $q->where('id', $id);
    }

    public function scopeHasCoords($q)
    {
        return $q->whereNotNull('latitude')->whereNotNull('longitude');
    }


    public function isRegistered(): bool
    {
        return (bool)$this->is_registered;
    }

    public function isOnline(): bool
    {
        return $this->hasFreshLog() || $this->hasFreshEvent();
    }

    public function isEnabled(): bool
    {
        return (bool)$this->is_enabled;
    }

    public function hasFreshLog($minutes = null)
    {
        $minutes = $minutes ?? config('device.offline_time');
        return $this->lastLog && $this->lastLog->created_at->diffInMinutes(Carbon::now()) < $minutes;
    }

    public function hasFreshEvent($minutes = null)
    {
        $minutes = $minutes ?? config('device.offline_time');
        return $this->lastEvent && $this->lastEvent->created_at->diffInMinutes(Carbon::now()) < $minutes;
    }

    public function canBeEnabled(): bool
    {
        if (!$this->isRegistered() || empty($this->project_id) || empty($this->partner_id)) {
            return false;
        }
        $availableLicensesCount = $this->partner->licenses()->ofProject($this->project_id)->sum('count');
        $enabledDevicesCount = $this->partner->devices()->enabled()->count();

        return $enabledDevicesCount < $availableLicensesCount;
    }

    public function validateRegisterCode(?string $code): bool
    {
        return !is_null($this->register_code) && $this->register_code === $code;
    }

    public function register(): self
    {
        $this->is_registered = true;
        if (empty($this->registered_at)) {
            $this->registered_at = now();
        }
        $this->register_code = null;

        return $this;
    }

    public function activate($partnerId = null, $projectId = null): self
    {

        $this->activation_code = null;
        $this->partner_id = $partnerId ?? auth()->user()->partner->id ?? null;
        $this->project_id = $projectId ?? null;
        if (empty($this->project_id)) {
            $partner = Partner::find($this->partner_id);
            $partnerProject = $partner ? $partner->projects()->first() : null;
            $this->project_id = $partnerProject->id ?? null;
        }

        return $this->register()->enable();
    }

    public function enable(bool $value = true): self
    {
        $this->is_enabled = $value && $this->canBeEnabled();
        return $this;
    }

    public function toArray()
    {
        $arr = parent::toArray();

        $additional = [
            'is_online' => $this->isOnline()
        ];

        return array_merge($arr, $additional);
    }
}
