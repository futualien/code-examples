<?php

namespace App\Models;

use App\Traits\HasRules;

class DeviceConfig extends Model
{
    use HasRules;

    const STATUS_EDITED = 1;
    const STATUS_SENT = 2;
    const STATUS_CHANGED = 3;
    const STATUS_ERROR = 4;

    const STATUSES = [
        'edited' => self::STATUS_EDITED,
        'sent' => self::STATUS_SENT,
        'changed' => self::STATUS_CHANGED,
        'error' => self::STATUS_ERROR
    ];

    protected $fillable = ['device_id', 'config'];

    protected $casts = ['config' => 'array'];

    protected $rules = [
        'create' => [
            'config' => 'required|array',
        ],
        'update' => [
            'config' => 'required|array',
        ],
    ];

    protected $appends = ['statuses'];


    protected static function boot()
    {
        parent::boot();

        self::saving(function ($model) {
            if ($model->isDirty('config')) {
                $model->markAsEdited();
            }
        });
    }


    public function getStatusesAttribute()
    {
        return array_flip(static::STATUSES);
    }


    public function device()
    {
        return $this->belongsTo(Device::class, 'device_id', 'id');
    }

    public function markAsEdited()
    {
        $this->status = static::STATUS_EDITED;
        return $this;
    }


    public function markAsSent()
    {
        $this->status = static::STATUS_SENT;
        return $this;
    }

    public function markAsChanged()
    {
        $this->status = static::STATUS_CHANGED;
        return $this;
    }

    public function markError()
    {
        $this->status = static::STATUS_ERROR;
        return $this;
    }
}
