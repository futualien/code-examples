<?php

namespace App\Models;

use App\Events\DoneUpdateConfigCommand;
use App\Events\SentUpdateConfigCommandToDevice;

/**
 * @property \Illuminate\Support\Carbon|null sent_at
 * @property \Illuminate\Support\Carbon|null start_at
 * @property \Illuminate\Support\Carbon|null done_at
 * @property \Illuminate\Support\Carbon|null cancel_at
 * @property integer user_id
 * @property string result
 * @property string message
 * @property array|null data
 */
class DeviceCommand extends Model
{
    protected $rules = [
        'create' => [
            'user_id' => 'sometimes|integer|exists:users,id',
            'device_id' => 'required|integer|exists:devices,id',
            'command_id' => 'required|integer|exists:commands,id',
            'command_body' => 'sometimes|nullable|string|min:1|max:255',
        ],
        'createMany' => [
            'commands' => 'required|array',
            'commands.*' => 'required|array',
            'commands.*.user_id' => 'sometimes|integer|exists:users,id',
            'commands.*.device_id' => 'required|integer|exists:devices,id',
            'commands.*.command_id' => 'required|integer|exists:commands,id',
            'commands.*.command_body' => 'sometimes|nullable|string|min:1|max:255',
        ],
        'update' => [
            'user_id' => 'sometimes|integer|exists:users,id',
            'device_id' => 'sometimes|integer|exists:devices,id',
            'command_id' => 'sometimes|integer|exists:commands,id',
            'command_body' => 'sometimes|nullable|string|min:1|max:255',
        ]
    ];

    protected $fillable = [
        'user_id', 'device_id', 'command_id', 'command_body'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'send_at', 'start_at', 'done_at', 'cancel_at'
    ];

    protected $casts = [
        'is_success' => 'boolean',
        'data' => 'array'
    ];


    protected static function boot()
    {
        parent::boot();

        self::updated(function ($model) {
            if (
                $model->wasChanged('sent_at') && !empty($model->sent_at)
                && $model->command && $model->command->alias === 'update_config'
            ) {
                event(new SentUpdateConfigCommandToDevice($model));
            }
            if (
                $model->wasChanged('done_at') && !empty($model->done_at)
                && $model->command && $model->command->alias === 'update_config'
            ) {
                event(new DoneUpdateConfigCommand($model));
            }
        });
    }

    public function device()
    {
        return $this->belongsTo(Device::class, 'device_id', 'id');
    }

    public function command()
    {
        return $this->belongsTo(Command::class, 'command_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }


    public function scopeUnsent($q)
    {
        return $q->whereNull('sent_at');
    }

    public function scopeNotCanceled($q)
    {
        return $q->whereNull('cancel_at');
    }

    public function getCommandBodyAttribute($value)
    {
        return $this->command->is_customizable
            ? $value
            : $this->command->command;
    }

    public function isNotStarted(): bool
    {
        return empty($this->start_at);
    }

    public function isNotFinished(): bool
    {
        return empty($this->done_at);
    }

    public function isSuccess(): bool
    {
        return $this->is_success;
    }

    public function hasError(): bool
    {
        return !$this->is_success;
    }

    public function belongsToUser(User $user)
    {
        return $this->user_id === $user->id;
    }

    public function setResult(string $text = null)
    {
        $this->result = $text;
        $this->is_success = ($text !== 'fail');
        return $this;
    }

    public function setMessage(string $text = null)
    {
        $this->message = $text;
        return $this;
    }

    public function setData(array $data = null)
    {
        $this->data = $data;
        return $this;
    }

    public function markAsSent($date = null)
    {
        $date = $date ?? now();
        $this->sent_at = $date;
        return $this;
    }

    public function markAsStarted($date = null)
    {
        $date = $date ?? now();
        $this->start_at = $date;
        return $this;
    }

    public function markAsFinished($date = null)
    {
        $date = $date ?? now();
        $this->done_at = $date;
        return $this;
    }

    public function cancel($date = null)
    {
        $date = $date ?? now();
        $this->cancel_at = $date;
        return $this;
    }
}
