<?php

namespace App\Models;

use App\Traits\HasRules;
use Illuminate\Database\Eloquent\Model as LaravelModel;

class Model extends LaravelModel
{
    use HasRules;
}
