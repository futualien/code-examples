<?php

namespace App\Models;

class Command extends Model
{
    protected $fillable = [
        'alias', 'command', 'is_active', 'is_customizable'
    ];

    protected $rules = [
        'create' => [
            'alias' => 'required|string|max:255',
            'command' => 'sometimes|nullable|string|max:255',
            'is_active' => 'sometimes|boolean',
            'is_customizable' => 'sometimes|boolean',
        ],
        'update' => [
            'alias' => 'sometimes|string|max:255',
            'command' => 'sometimes|nullable|string|max:255',
            'is_active' => 'sometimes|boolean',
            'is_customizable' => 'sometimes|boolean',
        ],
    ];

    public function deviceCommands()
    {
        return $this->hasMany(DeviceCommand::class, 'device_id', 'id');
    }

    public function scopeAlias($q, string $alias)
    {
        return $q->where('alias', $alias);
    }

    public function scopeActive($q, bool $value = true)
    {
        return $q->where('is_active', $value);
    }

    public function scopeCustomizable($q, bool $value = true)
    {
        return $q->where('is_customizable', $value);
    }
}
