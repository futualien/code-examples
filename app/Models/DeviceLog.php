<?php

namespace App\Models;

use App\Traits\Filterable;
use App\Traits\TableColumns;

/**
 * @property array log
 */
class DeviceLog extends Model
{
    use TableColumns, Filterable;

    protected $fillable = [
        'device_id', 'client_time', 'client_timezone', 'software_version', 'os', 'packages', 'load_avg', 'ip', 'local_ip', 'status', 'log'
    ];

    protected $casts = [
        'packages' => 'array',
        'log' => 'array'
    ];
    
    protected $dates = ['created_at', 'updated_at'];

    protected $filterable = [
        'created_date' => 'createdDate',
        'created_between' => 'createdBetween',
        'last_day' => 'lastDay'
    ];

    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;
    const STATUSES = [
        self::STATUS_DISABLED,
        self::STATUS_ENABLED,
    ];

    public function setStatusAttribute($value)
    {
        if (!in_array($value, self::STATUSES, true)) {
            switch ($value) {
                case 'on':
                    $value = self::STATUS_ENABLED;
                    break;
                case 'off':
                default:
                    $value = self::STATUS_DISABLED;
            }
        }
        $this->attributes['status'] = $value;
    }

    public function scopeCreatedAfter($q, $date, $equal = false) {
        return $q->where('created_at', $equal ? '>=' : '>', $date);
    }

    public function device()
    {
        return $this->belongsTo(Device::class, 'device_id', 'id');
    }
}