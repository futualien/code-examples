<?php

namespace App\Jobs;

use App\Models\DeviceEvent;
use App\Services\DeviceService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class DeviceNoConnection implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $deviceService;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(DeviceService $deviceService)
    {
        $this->deviceService = $deviceService;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $noConnectionDevices = $this->deviceService->getNoConnectionDevices();
        foreach ($noConnectionDevices as $device) {
            $this->deviceService->createOrUpdateDeviceOffline($device['id']);
        }
        $offlineDeviceIds = Arr::pluck($noConnectionDevices, 'id');
        $events = DeviceEvent::offline()->unfinished()->ignoreDevices($offlineDeviceIds)->update(['end_at' => now()->toDateTimeString()]);
    }
}
