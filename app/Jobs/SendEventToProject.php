<?php

namespace App\Jobs;

use App\Services\WebhookService;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class SendEventToProject implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 350;

    protected $url;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $url, array $data)
    {
        $this->url = $url;
        $this->data = $data;
    }

    public function retryAfter()
    {
        $seconds = config('webhooks.retryAfter', [0, 1, 60, 300, 1800]);
        $attempts = $this->attempts();
        return $seconds[$attempts] ?? $seconds[count($seconds) - 1];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WebhookService $webhookService)
    {
	Log::debug('job dispatched', [$this->url, $this->data]);

        try {
            $webhookService->send($this->url, $this->data);
        } catch (Exception $exception) {
	    Log::debug('error catched', [$exception->getMessage()]);
            $this->release(
                $this->retryAfter() // Dynamic retry delay after each failed attempt
            );
        }
    }
}
