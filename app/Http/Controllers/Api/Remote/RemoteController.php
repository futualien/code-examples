<?php

namespace App\Http\Controllers\Api\Remote;

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Requests\DeviceAuthRequest;
use App\Http\Requests\DeviceEventRequest;
use App\Http\Requests\DeviceRegisterRequest;
use App\Http\Requests\DeviceReportRequest;
use App\Http\Requests\DeviceScreenshotRequest;
use App\Http\Requests\DeviceStatusRequest;
use App\Services\CommandService;
use App\Services\DeviceService;
use App\Services\RepositoryService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RemoteController extends AuthController
{
    protected $service = DeviceService::class;

    public function auth(DeviceAuthRequest $request)
    {
        $credentials = $request->validated();
        try {
            $tokensData = $this->service->auth($credentials);
        } catch (\Exception $e) {
            return response()->json(['error' => 1, 'message' => $e->getMessage()]);
        }

        return response()->json(['error' => 0, 'data' => $tokensData]);
    }

    public function register(DeviceRegisterRequest $request)
    {
        try {
            $credentials = $request->validated();
            $tokensData = $this->service->register($credentials);
        } catch (\Exception $e) {
            return response()->json(['error' => 1, 'message' => $e->getMessage()]);
        }

        return response()->json(['error' => 0, 'data' => $tokensData]);
    }

    public function report(DeviceReportRequest $request)
    {
        try {
            $this->service->createLog($request->all());
        } catch (\Exception $exception) {
            return response()->json(['error' => 1, 'message' => $exception->getMessage()]);
        }

        return response()->json(['error' => 0]);
    }

    public function commands(CommandService $service)
    {
        $commands = $service->getNewDeviceCommands(auth()->id());

        return response()->json(['data' => $commands]);
    }

    public function vpn()
    {
        try {
            $config = $this->service->getVPNConfig();
        } catch (\Exception $e) {
            return response()->json(['error' => 1, 'message' => $e->getMessage()]);
        }

        return response()->json(['error' => 0, 'config' => base64_encode($config)]);
    }

    public function event(DeviceEventRequest $request, CommandService $service)
    {
        $validated = $request->validated();
        $service->registerEvent($validated);

        return response()->json(['error' => 0]);
    }

    public function download(Request $request, RepositoryService $repositoryService)
    {
        $validated = $request->validate($repositoryService->rules('download'));
        $softwareVersion = request()->header('Software-Version');
        try {
            $link = $repositoryService->getDownloadLink($validated['name'], $softwareVersion);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
        $disk = config('repository.filesystem.disk', config('filesystems.default', 'local'));
        return Storage::disk($disk)->download($link);
    }

    public function repository(RepositoryService $repositoryService)
    {
        $softwareVersion = request()->header('Software-Version');
        return response()->json([
            'files' => $repositoryService->getAuthDeviceProjectFiles($softwareVersion)
        ]);
    }

    public function config()
    {
        return response()->json([
            'config' => $this->service->getAuthDeviceConfig()
        ]);
    }

    public function screenshot(DeviceScreenshotRequest $request)
    {
        // TODO: implement the method
        return response()->json(['error' => 0]);
    }

    public function status(DeviceStatusRequest $request)
    {
        return response()->json(['error' => 0]);
    }
}
