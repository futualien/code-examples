<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use App\Models\DeviceCommand;
use App\Services\DeviceService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeviceCommandController extends Controller
{
    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('permission:read_device_commands')->only(['index', 'show']);
        $this->middleware('permission:create_device_commands')->only(['store', 'storeMany']);
        $this->middleware('permission:update_device_commands')->only(['update']);
        $this->middleware('permission:delete_device_commands')->only(['destroy']);
        $this->middleware('permission:cancel_device_commands')->only(['cancel']);
    }

    public function index()
    {
        $deviceId = request()->route()->parameter('device');
        return response()->json([
            'list' => $this->service->getDeviceCommands($deviceId)
        ]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->service->getDeviceCommandRules('create'));
        $deviceCommand = $this->service->createDeviceCommand($validated);
        return response()->json($deviceCommand, 201);
    }

    public function storeMany(Request $request)
    {
        $validated = $request->validate($this->service->getDeviceCommandRules('createMany'));
        $deviceCommands = $this->service->createDeviceCommands($validated);
        return response()->json($deviceCommands, 201);
    }

    public function destroy($deviceId)
    {
        $commandId = request()->route()->parameter('command');
        try {
            $deleted = $this->service->deleteDeviceCommand($commandId);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
        return response()->json(['deleted' => $deleted], 200);
    }

    public function cancel($deviceId, $commandId)
    {
        try {
            $canceledModel = $this->service->cancelDeviceCommand($commandId);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
        return response()->json(['command' => $canceledModel]);
    }
}
