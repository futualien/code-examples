<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use App\Services\DeviceService;
use Illuminate\Http\Request;

class DeviceLogController extends Controller
{
    protected $service = DeviceService::class;

    function __construct(Request $request)
    {
        parent::__construct($request);
        // TODO Не пропускает методы getDeviceLog(s). Почему-то якобы нет прав. Разобраться
        //$this->middleware('permission:read_device_logs')->only(['index', 'show', 'getDeviceLog', 'getDeviceLogs']);
        $this->middleware('permission:create_device_logs')->only(['store']);
        $this->middleware('permission:update_device_logs')->only(['update']);
        $this->middleware('permission:delete_device_logs')->only(['destroy']);
    }

    public function getDeviceLogs(Request $request, $deviceId) {
        return response()->json([
            'reports' => $this->service->getDeviceLogs($deviceId, $request->all())
        ]);
    }

    public function getDeviceLog(Request $request, $deviceId) {
        return response()->json([
            'report' => $this->service->getDeviceLog($deviceId, $request->all())
        ]);
    }
}
