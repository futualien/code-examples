<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('permission:read_devices')->only(['index', 'show', 'unregistered']);
        $this->middleware('permission:create_devices')->only(['store']);
        $this->middleware('permission:update_devices')->only(['update']);
        $this->middleware('permission:delete_devices')->only(['destroy']);
        $this->middleware('permission:activate_devices')->only(['activate']);
        $this->middleware('permission:enable_devices')->only(['enable']);
    }

    public function unregistered()
    {
        return response()->json([
            'list' => $this->service->getUnregisteredDevices()
        ]);
    }
    
    public function activate(Request $request)
    {
        try {
            $device = $this->service->activate($request->input('activation_code', ''));
            return response()->json(['error' => 0, 'device' => $device]);
        } catch(\Exception $e) {    
            return response()->json(['error' => 1, 'message' => $e->getMessage()]);
        }
        return response()->json(['error' => 1, 'message' => 'not_activated']);
    }

    public function enable(Request $request, $deviceId)
    {
        try {
            $device = $this->service->enable($deviceId, $request->get('is_enabled', false));
            return response()->json(['error' => 0, 'device' => $device]);
        } catch(\Exception $e) {    
            return response()->json(['error' => 1, 'message' => $e->getMessage()]);
        }
        return response()->json(['error' => 1, 'message' => 'not_enabled']);
    }
}
