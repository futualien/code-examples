<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use App\Services\DeviceService;
use Illuminate\Http\Request;

class DeviceEventController extends Controller
{
    protected $service = DeviceService::class;

    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('permission:read_devices')->only(['index', 'show', 'getDeviceEvents', 'getDeviceErrors']);
        $this->middleware('permission:create_devices')->only(['store']);
        $this->middleware('permission:update_devices')->only(['update']);
        $this->middleware('permission:delete_devices')->only(['destroy']);
    }

    public function getDeviceEvents($deviceId) {
        return response()->json([
            'list' => $this->service->getDeviceEvents($deviceId)
        ]);
    }

    public function getDeviceErrors($deviceId) {
        return response()->json([
            'list' => $this->service->getDeviceErrors($deviceId)
        ]);
    }
}
