<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use App\Services\DeviceService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class DeviceConfigController extends Controller
{
    protected $service = DeviceService::class;

    function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('permission:read_devices')->only(['index', 'show']);
        $this->middleware('permission:update_devices')->only(['update']);
        $this->middleware('permission:delete_devices')->only(['destroy']);
    }

    public function update(Request $request, $deviceId)
    {
        $validated = $request->validate($this->service->configRules('update'));
        try {
            $data = $this->service->saveConfig($deviceId, $validated);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
        return response()->json($data);
    }
}
