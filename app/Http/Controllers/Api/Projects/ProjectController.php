<?php

namespace App\Http\Controllers\Api\Projects;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\DeviceService;

class ProjectController extends Controller
{
    protected $service = DeviceService::class;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('projects.verify:get-project-devices')->only(['devices']);
        $this->middleware('projects.verify:get-project-device-info')->only(['getDeviceInfo']);
        $this->middleware('projects.verify:get-project-device-status')->only(['getDeviceStatus']);
        $this->middleware('projects.verify:get-project-device-report')->only(['getDeviceLastReport']);
        $this->middleware('projects.verify:get-project-devices-reports')->only(['reports']);
    }

    public function devices(Request $request)
    {
        $devices = $this->service->getProjectDevices($request->project_id, $request->fields);
        return response()->json([
            'devices' => $devices
        ]);
    }

    public function getDeviceInfo(Request $request, $deviceId)
    {
        $device = $this->service->getProjectDeviceInfo($request->project_id, $deviceId, $request->fields);
        return response()->json([
            'device' => $device
        ]);
    }

    public function getDeviceStatus($deviceId)
    {
        $status = $this->service->getDeviceStatus($deviceId);
        return response()->json([
            'status' => $status
        ]);
    }

    public function getDeviceLastReport(Request $request, $deviceId)
    {
        $report = $this->service->getDeviceLastReport($request->project_id, $deviceId, $request->fields);
        return response()->json([
            'report' => $report
        ]);
    }

    public function reports(Request $request) {
        $reports = $this->service->getDevicesLastReports($request->project_id, $request->fields);
        return response()->json([
            'reports' => $reports
        ]);
    }
}
