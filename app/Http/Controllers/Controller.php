<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class Controller extends BaseController
{
    protected $service;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(Request $request)
    {
        if (isset($this->service)) {
            $this->service = new $this->service;
        } else {
            $namespace = substr(get_called_class(), 0, strrpos(get_called_class(), "\\"));
            $route = $request->route();
            if ($route && $namespace === 'App\Http\Controllers\Api\Frontend') {
                $routeParts = explode('.', $route->getName());
                $service = Str::ucfirst($routeParts[count($routeParts) - 2]);
                $serviceNamespace = 'App\\Services\\';
                $servicePostfix = 'Service';
                $service = class_exists($serviceNamespace . $service . $servicePostfix)
                    ? $serviceNamespace . $service . $servicePostfix
                    : $serviceNamespace . Str::singular($service) . $servicePostfix;
                $this->service = new $service;
            }
        }
    }

    public function index()
    {
        return response()->json([
            'list' => $this->service->index()
        ]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate($this->service->rules('create'));
        try {
            $data = $this->service->create($validated);
        } catch (Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
        return response()->json($data, 201);
    }

    public function show(Request $request, $key)
    {
        try {
            $data = $this->service->show($key);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['error' => $exception->getMessage()], 404);
        }
        return response()->json($data);
    }

    public function update(Request $request, $key)
    {
        $validated = $request->validate($this->service->rules('update'));
        try {
            $data = $this->service->update($key, $validated);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['error' => $exception->getMessage()], 404);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
        return response()->json($data);
    }

    public function destroy($key)
    {
        try {
            $deleted = $this->service->delete($key);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }
        return response()->json(['deleted' => $deleted], 200);
    }
}
