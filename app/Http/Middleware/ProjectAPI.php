<?php

namespace App\Http\Middleware;

use App\Models\Project;
use App\Models\ProjectAPIKey;
use Closure;
use Illuminate\Support\Facades\Cache;

class ProjectAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        abort_if(!$request->filled('api_key'), 401, 'Unautorized. API key required.');

        $projectApiKey = Cache::remember('project_api_' . $request->api_key, 600, function () use ($request) {
            return ProjectAPIKey::active()->apikey($request->api_key)->with(['permissions'])->first();
        });

        abort_if(!$projectApiKey, 401, 'Unautorized. Incorrect API key.');

        $permission = $projectApiKey->permissions->where('alias', $permission)->first();

        abort_if(!$permission, 401, 'Unautorized. Missing required permission.');

        $request->merge([
            'project_id' => $projectApiKey->project_id,
            'fields' => $permission->pivot->fields ?? []
        ]);

        return $next($request);
    }
}
