<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeviceReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_time' => 'sometimes|date',
            'client_timezone' => 'sometimes|timezone',
            'software_version' => 'sometimes|string|max:255',
            'os' => 'sometimes|string|max:255',
            'packages' => 'sometimes|array',
            'load_avg' => 'sometimes|numeric',
            'status' => 'sometimes|string|max:255',
            'local_ip' => 'sometimes|string|max:255',
        ];
    }
}
