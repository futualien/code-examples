<?php

namespace App\Http\Requests;

use App\Models\DeviceEvent;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DeviceEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'command_id' => 'required_with:started_at,finished_at|integer|exists:device_commands,id',
            'started_at' => 'required_with:command_id|date|before_or_equal:now',
            'finished_at' => 'required_with:command_id|date|before_or_equal:now',
            'message' => 'required|string',
            'status' => 'required|string',
            'data' => 'sometimes|array',
            'type' => ['required', 'string', Rule::in(array_keys(DeviceEvent::TYPES))]
        ];
    }
}
