<?php

namespace App\Http\Requests;

use App\Rules\DeviceAuthHash;
use Illuminate\Foundation\Http\FormRequest;

class DeviceAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uuid' => ['bail', 'required', 'string', 'max:255'],
            'auth_hash' => ['bail', 'required', new DeviceAuthHash, 'string', 'min:64', 'max:64'],
            'user_id' => 'sometimes|required|integer|exists:users,id',
            'project_id' => 'sometimes|required|integer|exists:projects,id',
        ];
    }
}
