<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DeviceAuthHash implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return true; //hidden algorythm to check is auth hash valid
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('auth.auth_hash.incorrect');
    }
}
